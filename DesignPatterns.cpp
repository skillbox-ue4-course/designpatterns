﻿// DesignPatterns.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <vector>
#include "BuildingFactory.h"
#include "EnemyAbstractFactory.h"
#include "Builder.h"
#include "DatabaseSingleton.h"
#include "ZombiePrototype.h"

void TestFactoryMethod()
{
    std::cout << "Factory Method for Mining Buildings!\n";
    MinerBuildFactory* GoldBuilder = new GoldMinerBuilder();
    MinerBuildFactory* FuelBuilder = new FuelMinerBuilder();
    MinerBuildFactory* DiamondBuilder = new DiamondMinerBuilder();
    MinerBuildFactory* WoodBuilder = new WoodMinerBuilder();


    std::vector<MiningBuilding*> MineBuilding;
    MineBuilding.push_back(GoldBuilder->CreateBuilding(5, 20));
    MineBuilding.push_back(FuelBuilder->CreateBuilding(5, 15));
    MineBuilding.push_back(DiamondBuilder->CreateBuilding(2, 10));
    MineBuilding.push_back(WoodBuilder->CreateBuilding(10, 60));

    for (auto const build : MineBuilding) {
        build->Info();
    }

    for (auto const build : MineBuilding) {
        build->Work();
        build->Work();
        build->Upgrade();
    }

    for (auto const build : MineBuilding) {
        build->Info();
    }

    for (auto build : MineBuilding) {
        delete build;
    }

    delete GoldBuilder;
    delete FuelBuilder;
    delete DiamondBuilder;
    delete WoodBuilder;
}

Enemy* CreateNewEnemy(const EnemyAbstractFactory* factory, const EnemyType type, const std::string& name, int health, int attack)
{
    Enemy* new_enemy = nullptr;
    switch (type)
    {
    case EnemyType::FLY:
        new_enemy = factory->CreateFlyingEnemy(name, health, attack);
        break;
    case EnemyType::MILLE:
        new_enemy = factory->CreateMilleEnemy(name, health, attack);
        break;
    case EnemyType::RANGE:
        new_enemy = factory->CreateRangeEnemy(name, health, attack);
        break;
    default:
        break;
    }

    return new_enemy;
}

void TestEnemyAbstactFactory()
{
    EnemyAbstractFactory* OrcFactory = new OrcEnemyFactory();
    EnemyAbstractFactory* ElfFactory = new ElfEnemyFactory();

    std::vector<Enemy*> EnemyArmy;
    EnemyArmy.push_back(CreateNewEnemy(OrcFactory, EnemyType::FLY, "Aaab", 15, 5));
    EnemyArmy.push_back(CreateNewEnemy(OrcFactory, EnemyType::MILLE, "Aaff", 20, 4));
    EnemyArmy.push_back(CreateNewEnemy(OrcFactory, EnemyType::RANGE, "Abbaf", 10, 6));
    EnemyArmy.push_back(CreateNewEnemy(ElfFactory, EnemyType::FLY, "Caasf", 8, 6));
    EnemyArmy.push_back(CreateNewEnemy(ElfFactory, EnemyType::MILLE, "Certs", 10, 5));
    EnemyArmy.push_back(CreateNewEnemy(ElfFactory, EnemyType::RANGE, "Cuid", 7, 7));

    // attack by all enemy
    for (auto const en : EnemyArmy) {
        en->Attack();
    }

    // mass attack to enemy arme
    for (auto const en : EnemyArmy) {
        en->TakeDamage();
    }

    for (auto en : EnemyArmy) {
        delete en;
    }

    delete OrcFactory;
    delete ElfFactory;
}

void TestBuilder()
{
    Director* director = new Director();
    CarBuilder* builder = new CarBuilder();

    director->SetBuilder(builder);
    
    std::cout << "Simple car:\n";
    director->BuildSimpleCar();
    Machine* simple = builder->GetMachine();
    std::cout << (* simple) << '\n';

    std::cout << "Killer car:\n";
    director->BuildKillerCar();
    Machine* killer = builder->GetMachine();
    std::cout << (* killer) << '\n';

    delete simple;
    delete builder;
    delete director;
}

void TestDBSingleton()
{
    Database& db = Database::getInstance();
    db.WriteData();
    db.GetData();
}

void TestEnemyPrototype()
{
    Zombie* z1 = new Zombie();
    std::cout << "------------------------\n";
    std::cout << *z1 << '\n';

    Zombie* z2 = z1->clone();
    z2->SetName("asdasd");
    z2->SetAttack(7);
    std::cout << "------------------------\n";
    std::cout << *z2 << '\n';

    Zombie* z3 = z2->clone();
    z3->SetName("yvusbsdyv");
    std::cout << "------------------------\n";
    std::cout << *z3 << '\n';
}

int main()
{
    //TestFactoryMethod();
    //TestEnemyAbstactFactory();
    //TestBuilder();
    //TestDBSingleton();
    TestEnemyPrototype();
}
