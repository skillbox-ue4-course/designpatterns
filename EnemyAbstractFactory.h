#pragma once
#include <string>

enum class EnemyType
{
	MILLE,
	FLY,
	RANGE
};

// base enemy interface
class Enemy 
{
public:
	virtual ~Enemy() {};
	virtual void Attack() = 0;
	virtual void TakeDamage() = 0;

protected:
	// ctor not available outside, but can be reached by child
	Enemy(const EnemyType& type, const std::string& name, int health, int attack) 
		: m_Type(type), m_Name(name), m_Heath(health), m_AttackDamage(attack)
	{}

	// enemy info
	const EnemyType m_Type;
	const std::string m_Name;
	int m_Heath;
	int m_AttackDamage;
};

class Orc : public Enemy
{
public:
	Orc(const EnemyType& type, const std::string& name, int health, int attack)
		: Enemy(type, name, health, attack)
	{}

	void Attack() override;
	void TakeDamage() override;
};

class Elf : public Enemy
{
public:
	Elf(const EnemyType& type, const std::string& name, int health, int attack)
		: Enemy(type, name, health, attack)
	{}

	void Attack() override;
	void TakeDamage() override;
};

// create different enemies depending on location (orc capital, elf forest ...)
class EnemyAbstractFactory
{
public:
	virtual Enemy* CreateMilleEnemy(const std::string& name, int health, int attack) const = 0;
	virtual Enemy* CreateFlyingEnemy(const std::string& name, int health, int attack) const = 0;
	virtual Enemy* CreateRangeEnemy(const std::string& name, int health, int attack) const = 0;

	virtual ~EnemyAbstractFactory() {};
protected:
	EnemyAbstractFactory() {};
};

// factory for orc creation
class OrcEnemyFactory : public EnemyAbstractFactory
{
public:
	Enemy* CreateMilleEnemy(const std::string& name, int health, int attack) const override;
	Enemy* CreateFlyingEnemy(const std::string& name, int health, int attack) const override;
	Enemy* CreateRangeEnemy(const std::string& name, int health, int attack) const override;
};

// factory for elf creation
class ElfEnemyFactory : public EnemyAbstractFactory
{
public:
	Enemy* CreateMilleEnemy(const std::string& name, int health, int attack) const override;
	Enemy* CreateFlyingEnemy(const std::string& name, int health, int attack) const override;
	Enemy* CreateRangeEnemy(const std::string& name, int health, int attack) const override;

};

