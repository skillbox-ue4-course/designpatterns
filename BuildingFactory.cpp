#include "BuildingFactory.h"
#include <iostream>

int MiningBuilding::GetResouces() const
{
	return m_MiningValue;
}

// Can have more specific logic for each child
// but here we keep it simple
void MiningBuilding::Work()
{
	if (m_CurrentLoad < m_MaxHoldValue) {
		m_CurrentLoad += m_MiningValue;
	}
}

void MiningBuilding::Info() const
{
	std::cout << "Bilding Level - " << m_Lvl << '\n';
	std::cout << "Current Load - " << m_CurrentLoad << '\n';
	std::cout << "Free Space - " << m_MaxHoldValue - m_CurrentLoad << '\n';
	std::cout << "Income - " << m_MiningValue << '\n';
	std::cout << "/**************************/" << '\n';
}

void GoldMining::Upgrade()
{
	++m_Lvl;
	m_MaxHoldValue += 2 * m_MaxHoldValue;
	m_MiningValue *= 2;
}

void GoldMining::Info() const
{
	std::cout << "GoldMiner Info:\n";
	MiningBuilding::Info();
}

void FuelMining::Upgrade()
{
	++m_Lvl;
	m_MaxHoldValue += m_MaxHoldValue / 2;
	m_MiningValue += m_MiningValue / 2;
}

void FuelMining::Info() const
{
	std::cout << "FuelMiner Info:\n";
	MiningBuilding::Info();
}

void DiamondMiming::Upgrade()
{
	++m_Lvl;
	m_MaxHoldValue *= 2;
	m_MiningValue += 2;
}

void DiamondMiming::Info() const
{
	std::cout << "DiamondMimer Info:\n";
	MiningBuilding::Info();
}

void WoodMining::Upgrade()
{
	++m_Lvl;
	m_MaxHoldValue += 3 * m_MaxHoldValue;
	m_MiningValue *= 3;
}

void WoodMining::Info() const
{
	std::cout << "WoodMiner Info:\n";
	MiningBuilding::Info();
}

/*
* For simplicity all builders return simple object 
*/

MiningBuilding* GoldMinerBuilder::CreateBuilding(int mining_value, int max_hold_value) const
{
	return new GoldMining(mining_value, max_hold_value);
}

MiningBuilding* FuelMinerBuilder::CreateBuilding(int mining_value, int max_hold_value) const
{
	return new FuelMining(mining_value, max_hold_value);
}

MiningBuilding* WoodMinerBuilder::CreateBuilding(int mining_value, int max_hold_value) const
{
	return new WoodMining(mining_value, max_hold_value);
}

MiningBuilding* DiamondMinerBuilder::CreateBuilding(int mining_value, int max_hold_value) const
{
	return new DiamondMiming(mining_value, max_hold_value);
}