#pragma once

enum class MineType {
	GOLD,
	FUEL,
	DIAMOND,
	WOOD
};

// define main interface for mining building
class MiningBuilding
{
public:
	virtual ~MiningBuilding() {};

	// methods that should override
	virtual void Upgrade() = 0;
	virtual void Info() const;

	// common methods 
	void Work();
	int GetResouces() const;

protected:
	MiningBuilding(MineType type, int mining_value, int max_hold_val)
		: m_Type(type), m_MiningValue(mining_value), m_MaxHoldValue(max_hold_val)
	{}

	MineType m_Type;
	int m_Lvl = 1;
	int m_CurrentLoad = 0;
	int m_MiningValue;
	int m_MaxHoldValue;
};

class GoldMining : public MiningBuilding
{
public:
	GoldMining(int mining_value = 10, int max_hold_value = 20)
		: MiningBuilding(MineType::GOLD, mining_value, max_hold_value)
	{}

	void Info() const override;
	void Upgrade() override;
};

class FuelMining : public MiningBuilding
{
public:
	FuelMining(int mining_value = 5, int max_hold_value = 15)
		: MiningBuilding(MineType::FUEL, mining_value, max_hold_value)
	{}

	void Info() const override;
	void Upgrade() override;
};

class DiamondMiming : public MiningBuilding
{
public:
	DiamondMiming(int mining_value = 3, int max_hold_value = 9)
		: MiningBuilding(MineType::DIAMOND, mining_value, max_hold_value)
	{}

	void Info() const override;
	void Upgrade() override;
};

class WoodMining : public MiningBuilding
{
public:
	WoodMining(int mining_value = 15, int max_hold_value = 50)
		: MiningBuilding(MineType::WOOD, mining_value, max_hold_value)
	{}

	void Info() const override;
	void Upgrade() override;
};

// Factory for mining buildings
class MinerBuildFactory
{
protected:
	MinerBuildFactory() {};

public:
	virtual ~MinerBuildFactory() {};

	virtual MiningBuilding* CreateBuilding(int mining_value, int max_hold_value) const = 0;
};

// Concrete builder for GoldMine
class GoldMinerBuilder : public MinerBuildFactory
{
public:
	MiningBuilding* CreateBuilding(int mining_value, int max_hold_value) const override;
};

// Concrete builder for FuelMine
class FuelMinerBuilder : public MinerBuildFactory
{
public:
	MiningBuilding* CreateBuilding(int mining_value, int max_hold_value) const override;
};

// Concrete builder for WoodMine
class WoodMinerBuilder : public MinerBuildFactory
{
public:
	MiningBuilding* CreateBuilding(int mining_value, int max_hold_value) const override;
};

// Concrete builder for DiamondMine
class DiamondMinerBuilder : public MinerBuildFactory
{
public:
	MiningBuilding* CreateBuilding(int mining_value, int max_hold_value) const override;
};