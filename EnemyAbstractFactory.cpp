#include "EnemyAbstractFactory.h"
#include <iostream>

std::string TypeToStr(EnemyType type)
{
	if (type == EnemyType::FLY)
		return "Flying";
	else if (type == EnemyType::MILLE)
		return "Milee";
	else
		return "Range";
}

void Orc::Attack()
{
	std::cout << TypeToStr(m_Type) << ' ' << "Orc " << m_Name << " attack you\n";
}

void Orc::TakeDamage()
{
	std::cout << TypeToStr(m_Type) << ' ' << "Orc " << m_Name << " take damage\n";
}

void Elf::Attack()
{
	std::cout << TypeToStr(m_Type) << ' ' << "Elf " << m_Name << " attack you\n";
}

void Elf::TakeDamage()
{
	std::cout << TypeToStr(m_Type) << ' ' << "Elf " << m_Name << " take damage\n";
}


Enemy* OrcEnemyFactory::CreateMilleEnemy(const std::string& name, int health, int attack) const
{
	return new Orc(EnemyType::MILLE, name, health, attack);
}

Enemy* OrcEnemyFactory::CreateFlyingEnemy(const std::string& name, int health, int attack) const
{
	return new Orc(EnemyType::FLY, name, health, attack);
}
Enemy* OrcEnemyFactory::CreateRangeEnemy(const std::string& name, int health, int attack) const
{
	return new Orc(EnemyType::RANGE, name, health, attack);
}

Enemy* ElfEnemyFactory::CreateMilleEnemy(const std::string& name, int health, int attack) const
{
	return new Elf(EnemyType::MILLE, name, health, attack);
}

Enemy* ElfEnemyFactory::CreateFlyingEnemy(const std::string& name, int health, int attack) const
{
	return new Elf(EnemyType::FLY, name, health, attack);
}
Enemy* ElfEnemyFactory::CreateRangeEnemy(const std::string& name, int health, int attack) const
{
	return new Elf(EnemyType::RANGE, name, health, attack);
}