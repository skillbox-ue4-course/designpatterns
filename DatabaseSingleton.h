#pragma once
#include <iostream>

class Database
{
public:
    static Database& getInstance()
    {
        static Database instance;
        return instance;
    }

    void GetData()
    {
        std::cout << "Reading from db...\n";
    }

    void WriteData()
    {
        std::cout << "Writing data to db\n";
    }

private:
    Database() = default;
    ~Database() = default;
    Database(const Database&) = delete;
    Database& operator=(const Database&) = delete;

};