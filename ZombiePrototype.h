#pragma once
#include <string>
#include <iostream>

class Zombie
{
public:

	Zombie(std::string name = "Undefined", int health = 10, int attack = 4)
		: m_Name(name), m_Health(health), m_Attack(attack)
	{}

	void SetName(const std::string& new_name)
	{
		m_Name = new_name;
	}

	void SetAttack(int new_attack)
	{
		m_Attack = new_attack;
	}

	void SetHealth(int new_h)
	{
		m_Health = new_h;
	}

	Zombie* clone() const
	{
		return new Zombie(m_Name, m_Health, m_Attack);
	}

	friend std::ostream& operator<<(std::ostream& out, const Zombie& z);

private:
	std::string m_Name;
	int m_Health;
	int m_Attack;
};

std::ostream& operator<<(std::ostream& out, const Zombie& z)
{
	return out << "Zombie Ifno: \nName - " << z.m_Name << '\n' <<
		"Attack - " << z.m_Attack << '\n' <<
		"Health - " << z.m_Health;

}