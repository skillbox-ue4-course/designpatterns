#pragma once
#include <string>
#include <vector>
#include <iostream>

class Machine
{
public:
	std::vector<std::string> MachineParts;
};

std::ostream& operator<<(std::ostream& out, const Machine& m)
{
	out << "Machine Parts: ";
	for (const auto& p : m.MachineParts)
	{
		out << ' ' << p;
	}
	return out;
}


class IBuilder
{
public:
	virtual void SetEngine() const = 0;
	virtual void SetWheels() const = 0;
	virtual void SetGun() const = 0;
};

class CarBuilder : public IBuilder
{
private:
	Machine* m_Car;

public:
	CarBuilder()
	{
		Reset();
	}

	~CarBuilder()
	{
		delete m_Car;
	}

	Machine* GetMachine() {
		Machine* res = m_Car;
		Reset();
		return res;
	}

	void Reset()
	{
		m_Car = new Machine();
	}

	void SetEngine() const override
	{
		if (m_Car) {
			m_Car->MachineParts.push_back("Engine V8 280Hp");
		}
	}
	void SetWheels() const override
	{
		if (m_Car) {
			std::string wheel_info = "4 Wheels";
			m_Car->MachineParts.push_back(wheel_info);
		}
	}
	void SetGun() const override
	{
		if (m_Car) {
			m_Car->MachineParts.push_back("Placed Gun: Rocket Launcher");
		}
	}
};

class Director
{
private:
	IBuilder* m_Builder;

public:
	void SetBuilder(IBuilder* builder) {
		m_Builder = builder;
	}

	void BuildSimpleCar() {
		m_Builder->SetEngine();
		m_Builder->SetWheels();
	}

	void BuildKillerCar() {
		m_Builder->SetEngine();
		m_Builder->SetWheels();
		m_Builder->SetGun();
	}
};